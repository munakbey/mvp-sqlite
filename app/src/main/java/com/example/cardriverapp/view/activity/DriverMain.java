package com.example.cardriverapp.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.cardriverapp.R;
import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Driver;
import com.example.cardriverapp.presenter.PDriver;
import com.example.cardriverapp.presenter.adapter.DriverRVAdapter;

public class DriverMain extends AppCompatActivity {
    private RecyclerView.Adapter adapter;
    PDriver pDriver=new PDriver();
    ImageView imgDriverAdd;
    Button btnCar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drive_main);

       // pDriver.addDriver(new Driver(1,"name","surname",25566,1));
        Log.e("xx",pDriver.getDriverList().size()+"************");
        RecyclerView recyclerView = findViewById(R.id.recycler_view1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        imgDriverAdd=findViewById(R.id.imgDriverAdd);
        btnCar=findViewById(R.id.btnCar);

     // Log.e("xx",pDriver.getDriverList().size()+"###");
        adapter = new DriverRVAdapter(pDriver.getDriverList(), MyApp.getAppContext() );
        recyclerView.setAdapter(adapter);

        imgDriverAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyApp.getAppContext(),AddDriverActivity.class);
                startActivity(intent);
            }
        });

        btnCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(MyApp.getAppContext(),MainActivity.class);
                startActivity(intent);
            }
        });

    }
}
