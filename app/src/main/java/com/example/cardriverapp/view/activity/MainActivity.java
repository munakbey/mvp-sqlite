package com.example.cardriverapp.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;

import com.example.cardriverapp.R;
import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.model.pojo.Driver;
import com.example.cardriverapp.presenter.PCar;
import com.example.cardriverapp.presenter.PDriver;
import com.example.cardriverapp.presenter.adapter.CarRVAdapter;
import com.example.cardriverapp.view.fragment.Frag1;

import java.util.List;
import java.util.Locale;


public class MainActivity extends AppCompatActivity implements
    AdapterView.OnItemSelectedListener {
    private RecyclerView.Adapter adapter;
    private ImageView  imgAdd;
    PCar pCar=new PCar();
    List<Car> list=pCar.getCarList();
    Spinner lng;
    Button img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);

        imgAdd=findViewById(R.id.imgAdd);
        imgAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MyApp.getAppContext(),AddCarActivity.class);
                startActivity(intent);
            }
        });


         Log.e("xx",pCar.getCarList().size()+"###");
         adapter = new CarRVAdapter(pCar.getCarList(),MyApp.getAppContext() );
         recyclerView.setAdapter(adapter);
         img=findViewById(R.id.button2);

         Button btn=findViewById(R.id.button);
         btn.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {
                 // getFragment(new Frag1(MyApp.getAppContext()));*************+
                 Intent intent = new Intent(MyApp.getAppContext(), DriverMain.class);
                 startActivity(intent);
             }
         });
        lng=findViewById(R.id.lng);
        lng.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        String[] languages={"Language","English","Turkish"};
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,languages);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        lng.setAdapter(aa);

        img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adapter = new CarRVAdapter(pCar.getCarList(),MyApp.getAppContext() );
                recyclerView.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        if(i==2){
            changeLanguage("tr");
        }
        if(i==1){
            changeLanguage("");
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    private void changeLanguage(String language){
        Locale locale = new Locale(language); //locale i default locale yani türkçe yaptık. Artık değişkenler values paketinden alınacak
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,getBaseContext().getResources().getDisplayMetrics());
        finish();//mevcut acivity i bitir.
        startActivity(getIntent());//activity i baştan yükle
    }

   /* private void getFragment(Fragment fragment){
        //FrameLayout frameLayout=findViewById(R.id.frameLayout);
        FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameLayout,fragment);
        transaction.commit();
    }*/
}
