package com.example.cardriverapp.view.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cardriverapp.R;
import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.model.pojo.Driver;
import com.example.cardriverapp.presenter.PCar;
import com.example.cardriverapp.presenter.PDriver;

import java.util.ArrayList;
import java.util.HashMap;

public class AddDriverActivity extends AppCompatActivity implements
    AdapterView.OnItemSelectedListener {
    TextView txtName,txtSurname,txtNumber;
    String name,surname,number;
    Button btnAddDriver;
    PDriver pDriver=new PDriver();
    PCar pCar=new PCar();
    private HashMap<Integer,Integer> plateId=new HashMap();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_driver);

        txtName=findViewById(R.id.txtName);
        txtSurname=findViewById(R.id.txtSurname);
        txtNumber=findViewById(R.id.txtNumber);

        btnAddDriver=findViewById(R.id.btnAddDriver);


        Spinner spin = (Spinner) findViewById(R.id.spinner);
        spin.setOnItemSelectedListener((AdapterView.OnItemSelectedListener) this);

        String[] plates=new String[pCar.getCarList().size()];
        for (int i = 0; i < pCar.getCarList().size(); i++) {
            //list.add(pCar.getCarList().get(i).getPlate());
            plates[i]=pCar.getCarList().get(i).getPlate();
            plateId.put(i,pCar.getCarList().get(i).getId());
        }
        //Creating the ArrayAdapter instance having the country list
        ArrayAdapter aa = new ArrayAdapter(this,android.R.layout.simple_spinner_item,plates);
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //Setting the ArrayAdapter data on the Spinner
        spin.setAdapter(aa);
    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view,final int i, long l) {
        btnAddDriver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                name=txtName.getText().toString();
                surname=txtSurname.getText().toString();
                number=txtNumber.getText().toString();

                Log.e("xx",pDriver.getDriverList().size()+"^#");
                pDriver.addDriver(new Driver(name,surname,Long.valueOf(number),Integer.valueOf(plateId.get(i).toString())));
                Intent intent= new Intent(MyApp.getAppContext(),DriverMain.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
}
