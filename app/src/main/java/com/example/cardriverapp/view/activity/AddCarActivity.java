package com.example.cardriverapp.view.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.example.cardriverapp.R;
import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.presenter.PCar;

import java.util.Locale;

public class AddCarActivity extends AppCompatActivity {
    EditText txtMark,txtModel,txtPlate;
    String mark,model,plate;
    Button btnAddCar;
    PCar pCar=new PCar();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);

        txtMark=findViewById(R.id.txtModel);
        txtModel=findViewById(R.id.txtModel);
        txtPlate=findViewById(R.id.txtPlate);
        btnAddCar=findViewById(R.id.btnAddCar);

        btnAddCar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mark=txtMark.getText().toString();
                model=txtModel.getText().toString();
                plate=txtPlate.getText().toString();

                Log.e("xx",mark+"---"+model);
                pCar.addCar(new Car(mark,model,plate));
                Intent intent= new Intent(MyApp.getAppContext(),MainActivity.class);
                startActivity(intent);
            }
        });

    }


}
