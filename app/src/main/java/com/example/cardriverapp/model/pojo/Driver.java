package com.example.cardriverapp.model.pojo;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "driver",
        foreignKeys = @ForeignKey(entity = Car.class,
        parentColumns = "id",
        childColumns = "car_id",
                onDelete = ForeignKey.CASCADE,
                onUpdate = ForeignKey.CASCADE))
public class Driver {
    @PrimaryKey(autoGenerate = true)
    public int id;
    private String name;
    private String surname;
   //private Blob photo;
    private long number;
   /* @ForeignKey(entity = Car.class,
            parentColumns = "id",
            childColumns = "car_id",
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE)*/
    private int car_id;

    public Driver(/*int id,*/ String name, String surname, long number, int car_id) {
      //  this.id = id;
        this.name = name;
        this.surname = surname;
        this.number = number;
        this.car_id = car_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    public int getCar_id() {
        return car_id;
    }

    public void setCar_id(int car_id) {
        this.car_id = car_id;
    }
}
