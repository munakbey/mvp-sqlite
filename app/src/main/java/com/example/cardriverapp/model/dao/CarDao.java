package com.example.cardriverapp.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.cardriverapp.model.pojo.Car;

import java.util.List;

@Dao
public interface CarDao {
    @Insert
    void insertCar(Car car);

    @Update
    void updateCar(Car car);

    @Query("DELETE FROM car WHERE id = :id")
    void deleteCar(int id);

    @Query("DELETE FROM car")
    void deleteAll();

    @Query("SELECT * FROM car")
    List<Car> carList();

}