package com.example.cardriverapp.model.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.model.pojo.Driver;

import java.util.List;

@Dao
public interface DriverDao {
    @Insert
    void insertDriver(Driver driver);

    @Update
    void updateDriver(Driver driver);

    @Query("DELETE FROM driver WHERE id = :id")
    void deleteDriver(int id);

    @Query("SELECT * FROM driver")
    List<Driver> driverList();
}
