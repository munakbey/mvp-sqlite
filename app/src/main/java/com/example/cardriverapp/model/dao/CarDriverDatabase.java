package com.example.cardriverapp.model.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.model.pojo.Driver;

@Database(entities = {Car.class, Driver.class}, version = 2 ,exportSchema = false)
public abstract class CarDriverDatabase extends RoomDatabase {
    private static CarDriverDatabase instance;

    public static synchronized  CarDriverDatabase getInstance(Context context){
        if(instance==null){
            instance= Room.databaseBuilder(context,
                    CarDriverDatabase.class,"car-driver")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return instance;
    }

    public abstract CarDao getCarDao();
    public abstract DriverDao getDriverDao();

}
