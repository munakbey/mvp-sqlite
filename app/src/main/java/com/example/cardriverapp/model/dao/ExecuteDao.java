package com.example.cardriverapp.model.dao;

import android.content.Context;
import android.os.AsyncTask;
import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.model.pojo.Driver;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static com.example.cardriverapp.common.MyApp.getAppContext;

public class ExecuteDao {
    private CarDao daoCar;
    private DriverDao daoDriver;
    private Context mContext;


    public ExecuteDao() {
        mContext = getAppContext();
        daoCar = CarDriverDatabase.getInstance(mContext).getCarDao();
        daoDriver=CarDriverDatabase.getInstance(mContext).getDriverDao();
    }

    public List<Car> listCar(/*Car car*/) {
        List<Car> list=null;
        try {
            list = new GetAllCarAsyncTask().execute().get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addCar(Car car) {
        try {
             new AddCarAsyncTask().execute(car).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteCar(int id) {
        try {
            new DeleteCarAsyncTask().execute(id).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll() {
        try {
            new DeleteAllAsyncTask().execute().get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAllCarAsyncTask extends AsyncTask<Void, Void, List<Car>> {
        @Override
        protected List<Car> doInBackground(Void... voids) {
            return daoCar.carList();
        }
    }

    private class AddCarAsyncTask extends AsyncTask<Car, Void, Car> {

        @Override
        protected Car doInBackground(Car... cars) {
            daoCar.insertCar(cars[0]);
            //  daoCar.insertCar(new Car(7,"mark4","model4","26XX5236"));
            return null;
        }
    }

    private class DeleteCarAsyncTask extends AsyncTask<Integer,Void,Void>{
        @Override
        protected Void doInBackground(Integer... ıntegers) {
            daoCar.deleteCar(ıntegers[0]);
            return null;
        }
    }

    private class DeleteAllAsyncTask extends  AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            daoCar.deleteAll();
            return null;
        }
    }
 //-----------------------------------------------------------------------------------

    public List<Driver> listDriver(){
        List<Driver> list=null;
        try {
            list = new GetAllDriverAsyncTask().execute().get(5, TimeUnit.SECONDS);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }

    public void addDriver(Driver driver){
        try {
            new AddDriverAsyncTask().execute(driver).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteDriver(int id) {
        try {
            new DeleteDriverAsyncTask().execute(id).get();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private class GetAllDriverAsyncTask extends AsyncTask<Void, Void, List<Driver>> {
        @Override
        protected List<Driver> doInBackground(Void... voids) {
            return daoDriver.driverList();
        }
    }

    private class AddDriverAsyncTask extends  AsyncTask<Driver, Void, Driver>{
        @Override
        protected Driver doInBackground(Driver... drivers) {
            daoDriver.insertDriver(drivers[0]);
            return null;
        }
    }

    private class DeleteDriverAsyncTask extends AsyncTask<Integer,Void,Void> {
        @Override
        protected Void doInBackground(Integer... ıntegers) {
            daoDriver.deleteDriver(ıntegers[0]);
            return null;
        }
    }
}