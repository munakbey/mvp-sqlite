package com.example.cardriverapp.common;

import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.cardriverapp.R;
import com.example.cardriverapp.model.dao.ExecuteDao;
import com.example.cardriverapp.presenter.PCar;

public class MyApp extends Application {
    private static Context context;
    private static ExecuteDao executer;

    @Override
    public void onCreate() {
     super.onCreate();
     context=getApplicationContext();
    }

    public static Context getAppContext(){
        return MyApp.context;
    }

    public static ExecuteDao executer(){
        if(executer==null){
            executer=new ExecuteDao();
        }
        return  executer;
    }
}
