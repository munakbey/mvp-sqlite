package com.example.cardriverapp.presenter.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.example.cardriverapp.R;
import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Car;
import com.example.cardriverapp.model.pojo.Driver;
import java.util.ArrayList;
import java.util.List;

public class DriverRVAdapter extends RecyclerView.Adapter<DriverRVAdapter.MyViewHolder> {

    public List<Driver> listItem;
    //ModulesListRV_adapter thisAdapter;
    Context context;
    //PModulesManager manager;

    public DriverRVAdapter(List<Driver> listItem, Context context) {
        this.listItem = listItem;
        this.context = context;
    }

    @NonNull
    @Override
    public DriverRVAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.itemfordriver, parent, false);
        return new DriverRVAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final DriverRVAdapter.MyViewHolder holder,final int position) {
        holder.txtName.setText(listItem.get(position).getName());
        holder.txtSurname.setText(listItem.get(position).getSurname());
        holder.txtNumber.setText(String.valueOf(listItem.get(position).getNumber()));
        holder.dlinearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                holder.imgDeleteDriver.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Log.e("xx",listItem.get(position).getId()+"");
                        MyApp.executer().deleteDriver(listItem.get(position).getId());
                        MyApp.executer().listCar();
                        notifyDataSetChanged();
                    }
                });
                Log.e("xx",listItem.get(position).getId()+"!!!!");
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setDriver(List<Driver> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtName,txtSurname,txtNumber;
        LinearLayout dlinearLayout;
        ImageView imgDeleteDriver;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName=itemView.findViewById(R.id.txtName);
            txtSurname=itemView.findViewById(R.id.txtSurname);
            txtNumber=itemView.findViewById(R.id.txtNumber);
            dlinearLayout=itemView.findViewById(R.id.dlinearLayout);
            imgDeleteDriver=itemView.findViewById(R.id.imgDeleteDriver);
        }
    }
}
