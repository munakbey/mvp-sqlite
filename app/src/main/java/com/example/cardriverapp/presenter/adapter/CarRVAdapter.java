package com.example.cardriverapp.presenter.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cardriverapp.R;
import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Car;
import java.util.ArrayList;
import java.util.List;


public class CarRVAdapter extends RecyclerView.Adapter<CarRVAdapter.MyViewHolder>   {

    public List<Car> listItem = new ArrayList<>();
    //ModulesListRV_adapter thisAdapter;
    Context context;
    //PModulesManager manager;

    public CarRVAdapter(List<Car> listItem, Context context) {
        this.listItem = listItem;
        this.context = context;
    }

    @NonNull
    @Override
    public CarRVAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final CarRVAdapter.MyViewHolder holder,final int position) {
        holder.txtMark.setText(listItem.get(position).getMark());
        holder.txtModel.setText(listItem.get(position).getModel());
        holder.txtPlate.setText(listItem.get(position).getPlate());
        holder.linearLayout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                    holder.btnDelete.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Log.e("xx",listItem.get(position).getId()+"");
                            MyApp.executer().deleteCar(listItem.get(position).getId());
                           // MyApp.executer().deleteAll();
                            listItem=MyApp.executer().listCar();
                            notifyDataSetChanged();
                          //  MainActivity activity = (MainActivity)context;
                        }
                    });
                Log.e("xx",listItem.get(position).getId()+"!!!!");
            }
        });

    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public void setCar(List<Car> listItem) {
        this.listItem = listItem;
        notifyDataSetChanged();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtPlate,txtMark,txtModel;
        LinearLayout linearLayout;
        ImageView btnDelete;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtPlate=itemView.findViewById(R.id.txtPlate);
            txtMark=itemView.findViewById(R.id.txtMarka);
            txtModel=itemView.findViewById(R.id.txtModel);
            linearLayout=itemView.findViewById(R.id.linearLayout);
            btnDelete=itemView.findViewById(R.id.imgDelete);
        }
    }
}
