package com.example.cardriverapp.presenter;

import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.dao.ExecuteDao;
import com.example.cardriverapp.model.pojo.Car;

import java.util.List;

public class PCar  {

    public List<Car> getCarList(){
        return MyApp.executer().listCar();
    }

    public void addCar(Car car){
        MyApp.executer().addCar(car);
    }
}
