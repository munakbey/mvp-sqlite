package com.example.cardriverapp.presenter;

import com.example.cardriverapp.common.MyApp;
import com.example.cardriverapp.model.pojo.Driver;

import java.util.List;

public class PDriver {
    public List<Driver> getDriverList(){
        return MyApp.executer().listDriver();
    }

    public void addDriver(Driver driver) {
        MyApp.executer().addDriver(driver);
    }
}
